const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');
const multer = require('multer');
const { body, validationResult } = require('express-validator');

const app = express();
const USER_FILE_PATH = path.join(__dirname, 'users.json');

app.use(bodyParser.json());

const fetchUsers = () => {
  try {
    const data = fs.readFileSync(USER_FILE_PATH, 'utf8');
    return JSON.parse(data);
  } catch (err) {
    return [];
  }
}

const saveUsers = (users) => {
  fs.writeFileSync(USER_FILE_PATH, JSON.stringify(users, null, 2), 'utf8');
}

app.get('/users', (req, res) => {
  const users = fetchUsers();
  res.json({ users });
});

app.post('/users', 
[ body('first_name').isLength({min:3}).notEmpty(), body('last_name').isLength({min:3}), body('email').isEmail().isLength({min:10}).notEmpty(), body('phone_no').isNumeric().isLength({ min: 10 }).notEmpty(), body('gender').isIn(['male', 'female', 'others']).notEmpty(), body('address').isLength({ max: 100 }),],
(req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  next();
},
(req, res) => {
  const users = fetchUsers();
  const newUser = { id: users.length + 1, ...req.body };
  users.push(newUser);
  saveUsers(users);
  
  res.status(201).json({ message: 'User created success', user: newUser });
});

app.put('/users/:userId', 
[ body('first_name').isLength({min:3}).notEmpty(), body('last_name').isLength({min:3}), body('email').isEmail().isLength({min:10}).notEmpty(), body('phone_no').isNumeric().isLength({ min: 10 }).notEmpty(), body('gender').isIn(['male', 'female', 'others']).notEmpty(), body('address').isLength({ max: 100 }),],
(req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  next();
},
(req, res) => {
    const users = fetchUsers();
    const userId = parseInt(req.params.userId);
    const updatedUser = { ...req.body, id: userId };

    const updatedUsers = users.map((user) => (user.id === userId ? updatedUser : user));
    saveUsers(updatedUsers);

    res.json({ message: 'User updated!', user: updatedUser });
});

app.delete('/users/:userId', (req, res) => {
  const users = fetchUsers();
  const userId = parseInt(req.params.userId);
  const updatedUsers = users.filter((user) => user.id !== 1);
  console.log(updatedUsers);
  saveUsers(updatedUsers);
  res.json({message:`User deleted! ${userId}`});
});

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    fs.mkdirSync(path.join(__dirname,'uploads'), { recursive: true });
    cb(null, path.join(__dirname,'uploads'));
  },
  filename: (req, file, cb) => {
    cb(null, `${Date.now()}-${file.originalname}`);
  },
});

const upload = multer({ storage });

app.post('/users/:userId/upload-image', upload.single('image'), (req, res) => {
  console.log(req)
  const userId = parseInt(req.params.userId);
  const users = fetchUsers();
  const user = users.find((u) => u.id === userId);
  if (!user) {
    return res.status(404).json({error:'User not found'});
  }
  const imagePath = req.file?path.join('uploads', req.file.filename):null;
  users.map((data) => {
    if(data.id == userId){
        data.path = imagePath
    }
    return data
  })
  saveUsers(users);
  res.json({message:'Image uploaded!',imagePath });
});

app.listen(3000, () => {
  console.log(`url: http://localhost:3000`);
});

